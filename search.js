var data = require('./publications_small.json');

var itemsjs = require('itemsjs')(data, {
  sortings: {
    name_asc: {
      field: 'name',
      order: 'asc'
    }
  },
  aggregations: {
    authors: {
      title: 'Authors',
      size: 10
    },
    published_year: {
      title: 'published_year',
      size: 10
    }
  },
  searchableFields: ['title', 'published_year','authors']
});

/**
 * get filtered list of movies
 */
var publications = itemsjs.search({
  per_page: 4,
  sort: 'name_asc',
  // full text search
   query: '2018',
  // filters: {
  //   published_year: ['2018']
  // }
})
console.log(JSON.stringify(publications, null, 2));

/**
 * get list of top tags
 */
// var top_tags = itemsjs.aggregation({
//   name: 'published_year',
//   per_page: 10
// })
// console.log(JSON.stringify(top_tags, null, 2));
